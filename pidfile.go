// pidfile
package daemonizer

/*
Модуль работы с pid-файлами
*/

import (
	"errors"
	"fmt"
	"io"
	"os"
)

var EProcRunning = errors.New("Process already running")

type TPidFile struct {
	Pid   int
	fName string
	File  *os.File
}

/*
	Получить процесс из лок-файла
*/
func GetProcess(fName string) (*os.Process, error) {
	var (
		f   *os.File
		p   *os.Process
		pid int
		err error
	)
	if f, err = os.OpenFile(fName, os.O_RDONLY, os.ModeTemporary|0640); err != nil {
		// Ошибка открытия файла
		return nil, errors.New("os.OpenFile" + err.Error())
	}
	if _, err = fmt.Fscanf(f, "%d\n", &pid); err != nil {
		// Что-то не так
		return nil, errors.New("fmt.FScanf: " + err.Error())
	}
	if p, err = os.FindProcess(pid); err != nil {
		// Процесс не найден?
		return nil, errors.New("os.FindProcess: " + err.Error())
	}
	return p, nil
}

func NewPidFile(fName string) (pid *TPidFile, err error) {
	pid = &TPidFile{
		fName: fName,
		Pid:   os.Getpid(),
	}
	if pid.File, err = os.OpenFile(fName, os.O_CREATE|os.O_RDWR, os.ModeTemporary|0640); err == nil {
		if _, err = fmt.Fscanf(pid.File, "%d\n", &pid.Pid); (err == nil) || (err == io.EOF) {
			if pid.Pid != os.Getpid() {
				if IsProcessRunning(pid.Pid) {
					return nil, EProcRunning
				}
			}
			pid.File.Seek(0, 0)
			var n int
			if n, err = fmt.Fprintf(pid.File, "%d\n", os.Getpid()); err == nil {
				pid.File.Truncate(int64(n))
				return pid, nil
			} else {
				return nil, errors.New("Error write to pid file: " + err.Error())
			}
		} else {
			return nil, errors.New("Error read from pid file: " + err.Error())
		}
	} else {
		return nil, errors.New("Error create|open pid file: " + err.Error())
	}

}

func (pid *TPidFile) Release() (err error) {
	if err = pid.File.Close(); err != nil {
		return errors.New("File.Close(" + pid.fName + "): " + err.Error())
	}
	if err = os.Remove(pid.fName); err != nil {
		return errors.New("File.Remove(" + pid.fName + "): " + err.Error())
	}
	return nil
}
