# Утилиты для демонизации приложений на GO

Актуальная информация: http://wiki.leolab.info/lib/daemonizer.go/

## Функции

### GetProcess
```
func GetProcess(string) (*os.Process,error) {}
```

### IsProcessRunning
```
func IsProcessRunning(int) bool {}
```

### Fork
```
func Fork() (int,error){}
```

## Классы

### TPidFile
```
type TPidFile struct{
  Pid int
  File *os.File
}

func NewPidFile(string) (*TPidFile,error) {}
func (*TPidFile) Release() {}

```

#### Использование
```
pid,err:=NewPidFile("/var/run/MyApp.pid")
if err!=nil {
  panic(err)
}
defer pid.Release()

```

## Пример использования пакета

```
var(
  err error
  p *os.Process
  pidf *TPidFile
  pid int
)

if p,err = GetProcess("/var/run/myapp.pid");err!=nil { panic(err) } // Невозможно открыть файл, получить процесс, etc...
if IsProcessRunning(p.Pid) { panic("процесс уже запущен") }
if pid,err = Fork();err!=nil { panic(err) }
if pid!=0 { os.Exit(0) } // Мы родительский процесс, завершаемся.
if pidf,err = NewPidFile("/var/run/myapp.pid");err!=nil { panic(err) } // Ошибка создания pid-файла
defer pidf.Release()
// Код демона
```