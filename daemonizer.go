package daemonizer

/*
	Основной модуль демонизации
*/

import (
	"errors"
	"os"
	"strconv"
	"syscall"
)

const (
	Version string = "0.1.4"
	Date    string = "31/03/2018"
)

/*
	Проверка существования (живости) процесса по pId
*/
func IsProcessRunning(pid int) bool {
	p, _ := os.FindProcess(pid)
	err := p.Signal(syscall.Signal(0))
	return err == nil
}

/*
	Реализация fork()
	Новому процессу возвращается значение 0
	Родительскому - pId порожденного
*/
func Fork() (pid int, err error) {

	var (
		args    []string
		env     []string
		child   *os.Process
		attr    *os.ProcAttr
		ExeName string
	)

	//@TODO[]: Переменая должна изменяться, для предотвращения некорректных запусков цепочек
	//@UPD: Должна быть возможность выбора, запускать процесс в своем контексте, или создавать новый
	if os.Getenv("_pParent_") != "" {
		//= Закрываем стандартные файлы >
		if err = os.Stdin.Close(); err != nil {
			return 0, errors.New("stdin.Close(): " + err.Error())
		}
		if err = os.Stdout.Close(); err != nil {
			return 0, errors.New("stdout.Close(): " + err.Error())
		}
		if err = os.Stderr.Close(); err != nil {
			return 0, errors.New("stderr.Cloas(): " + err.Error())
		}
		//< Закрываем стандартные файлы =
		return 0, nil
	}

	args = os.Args
	env = os.Environ()
	env = append(env, "_pParent_="+strconv.Itoa(os.Getpid()))

	if ExeName, err = os.Executable(); err != nil {
		return 0, errors.New("os.Executable: " + err.Error())
	}
	attr = &os.ProcAttr{
		Env: env,
		Files: []*os.File{
			os.Stdin,
			os.Stdout,
			os.Stderr,
		},
		Sys: &syscall.SysProcAttr{
			Noctty:     true,
			Setsid:     false,
			Setctty:    false,
			Foreground: false,
		},
	}

	if attr.Dir, err = os.Getwd(); err != nil {
		return 0, errors.New("os.Getwd: " + err.Error())
	}

	if child, err = os.StartProcess(ExeName, args, attr); err != nil {
		return 0, errors.New("os.StartProcess: " + err.Error())
	}
	return child.Pid, nil
}

/*
	Демонизация процесса
*/
func Daemonize() {

}

/*
	Завершение процесса
*/
func Release() {}
